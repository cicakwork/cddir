**CDDIR**

this code is for moving to projects folder and 2 subfolder within the project folder
---

## How to use?

you have to have a folder name "projects" within your documents folder

before you call the batch, don't forget to include the batch folder in your environment variable
in command prompt, call the file name..

```
howto-> cbm p [subfolder1] [subfolder2]
```