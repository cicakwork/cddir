@echo off

set "Dir[0]=C:\Users\[username]\Documents\projects"

if [%1]==[] (
    goto prompt
) else (
    set "a=%1"
    goto :skip
)

:prompt
echo.
set /p a=Enter param: 

:skip


if [%a%]==[p] (
    cd /D %Dir[0]%
    if [%2]==[] (
        EXIT /B 0
    ) else (
        cd /D %2
        if [%3]==[] (
            EXIT /B 0
        ) else (
            cd /D %3
            EXIT /B 0
        )
        EXIT /B 0
    )
) else (
    goto :prompt
)




